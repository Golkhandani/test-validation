import { ModelDefinition, Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import mongoose, { Document, SchemaOptions, Types } from 'mongoose';
import { v4 as uuid } from "uuid";

const schemaOption = {
    timestamps: true,
    autoIndex: true,
    versionKey: false
}
export enum TranslationEngineAvailability {
    AVAILABLE = "AVAILABLE",
    NOT_AVAILABLE = "NOT_AVAILABLE"
}

export enum OrderStatus {
    OPEN = "open",
    CONFIRMED = "confirmed",
    IN_PROGRESS = "in_progress",
    CLIENT_REVIEW = "client_review",
    CANCELED = "canceled",
    DONE = "done"
}

export enum OrderPriority {
    NORMAL = "normal",
    URGENT = "urgent"
}

export enum OrderType {
    OFFICIAL = "official",
    UNOFFICIAL = "unofficial",
    NOTARIZED = "notarized"
}

export class TranslationType {
    @Prop(raw({
        normal: {
            type: Number, default: 1
        },
        urgent: {
            type: Number, default: 1
        },
        index: true,
    }))
    estimatedTime: {
        normal: number,
        urgent: number
    }
}


@Schema(schemaOption)
export class TranslationEngine {
    @Prop({
        type: String,
    })
    title: string;

    @Prop({
        type: String,
    })
    description: string;

    @Prop({
        index: true,
        type: String,
        enum: Object.values(TranslationEngineAvailability),

    })
    availability: TranslationEngineAvailability

    @Prop({
        index: true,
        type: String,
    })
    from: string;

    @Prop({
        index: true,
        type: String,
    })
    to: string;


    @Prop({
        default: null
    })
    official: TranslationType | null;
    @Prop({
        default: null
    })
    unofficial: TranslationType | null;
    @Prop({
        default: null
    })
    notarized: TranslationType | null;

}
export type TranslationEngineDocument = TranslationEngine & Document;
const TranslationEngineSchema = SchemaFactory.createForClass(TranslationEngine);
TranslationEngineSchema.index({ from: 1, to: 1 }, { unique: true });
export const TranslationEngineModel: ModelDefinition = { name: TranslationEngine.name, schema: TranslationEngineSchema }


export type PricingUnit = "word" | "page" | "term"
export class Priority<T> {
    normal: T;
    urgent: T
}


@Schema(schemaOption)
export class Category {
    @Prop({
        type: Types.ObjectId,
        ref: Category.name,
    })
    parent: Category | Types.ObjectId;

    @Prop({
        type: Types.ObjectId,
        ref: TranslationEngine.name,
        required: true
    })
    engine: TranslationEngine | string;
    @Prop({
        required: true
    })
    title: string;
    @Prop()
    pricing: {
        official?: Partial<
            Record<
                PricingUnit, Priority<number>
            >
        >;
        unofficial?: Partial<
            Record<
                PricingUnit, Priority<number>
            >
        >;
        notarized?: Partial<
            Record<
                PricingUnit, Priority<number>
            >
        >;
    };

}
export type CategoryDocument = Category & Document;
const CategorySchema: mongoose.Schema<Order> = SchemaFactory.createForClass(Category);
export const CategoryModel: ModelDefinition = { name: Category.name, schema: CategorySchema };


@Schema(schemaOption)
export class Order {
    @Prop({
        type: Types.ObjectId,
        ref: TranslationEngine.name,
        required: true
    })
    engine: TranslationEngine | string;

    @Prop({
        default: () => uuid().split("-")[0].toUpperCase()
    })
    shortId?: string;

    @Prop({
        type: Types.ObjectId,
        index: true
    })
    user: string;

    @Prop({
        enum: Object.values(OrderStatus)
    })
    status: OrderStatus;

    @Prop({
        enum: Object.values(OrderPriority)
    })
    priority: OrderPriority;

    @Prop({
        default: new Date()
    })
    deliveryTime: Date;

    @Prop({
        default: 0
    })
    totalPrice: number;

    @Prop({
        default: "New Order"
    })
    description: string;
    @Prop({
        enum: Object.values(OrderType)
    })
    type: OrderType;


}
export type OrderDocument = Order & Document;
const OrderSchema: mongoose.Schema<Order> = SchemaFactory.createForClass(Order);
export const OrderModel: ModelDefinition = { name: Order.name, schema: OrderSchema };


@Schema()
export class orderItemPricing {
    @Prop()
    word?: number;
    @Prop()
    page?: number;
    @Prop()
    term?: number;


}

@Schema(schemaOption)
export class OrderItem extends orderItemPricing {
    @Prop()
    file: string;

    @Prop({
        type: Types.ObjectId,
        ref: Order.name,
        required: true
    })
    order: Order | string;

    @Prop({
        type: Types.ObjectId,
        ref: Category.name,
        required: true
    })
    category: Category | string;

    @Prop()
    price: number;


}
export type OrderItemDocument = OrderItem & Document;
const OrderItemSchema: mongoose.Schema<Order> = SchemaFactory.createForClass(OrderItem);
export const OrderItemModel: ModelDefinition = { name: OrderItem.name, schema: OrderItemSchema };
