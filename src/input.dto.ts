
import { loadPackage } from '@nestjs/common/utils/load-package.util';
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import FastestValidator, { ValidationError } from "fastest-validator";
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

import {
    Schema,
    Array,
    Nested,
    Enum,
    Email,
    Number,
    getSchema,
    String, getCompiled

} from "./validation-decorators";
import { ApiProperty } from '@nestjs/swagger';
const checkType = (value, errors, schema, path, parent, obj) => {
    const isValid = Boolean((obj.data as Entity2).prop5);
    if (!isValid) {
        errors.push({
            field: "type",
            message: "You should at least add one 'type' (official, unofficial, notarized) for engine",
            type: "condition"
        })
    }

}

enum OneTwo {
    ONE = "ONE",
    TWO = "TWO"
}


@Schema({
    strict: true,
})
export class Entity1 {
    @Array({ items: "string" })
    @ApiProperty({
        example: ["Hi"],
    })
    prop1: string[];
}

@Schema({
    strict: true,
    messages: {},
    condition: checkType
})
export class Entity2 {
    @String({
        min: 2, max: 10, optional: true
    })
    @ApiProperty()
    prop0: string;

    @String()
    @ApiProperty()
    prop1: string;

    @Enum({ values: Object.values(OneTwo) })
    @ApiProperty()
    prop2: OneTwo;

    @Email()
    @ApiProperty()
    prop3: string;

    @Number({ positive: true })
    @ApiProperty()
    prop4: number;


    @Nested({
        optional: true
    })
    @ApiProperty({
        type: Entity1
    })
    prop5: Entity1;

}


@Schema({
    strict: true,
    messages: {},
    condition: checkType
})
export class Entity3 {
    @Number({ positive: true })
    @ApiProperty()
    prop4: number;


    @Nested({
        optional: true
    })
    @ApiProperty({
        type: Entity1
    })
    prop5: Entity1;

}

@Schema({
    strict: true
})
export class InputParam {
    @Number({ positive: true })
    @ApiProperty()
    id: number;
}


export class ValidationException extends Error {
    public errors = null;
    public status = 400;
    constructor(errors: any[]) {
        super();
        this.errors = errors;
        this.message = "Validation failed";
    }
    public getStatus() {
        return this.status;
    }
}



@Injectable()
export class ValidationPipe implements PipeTransform {
    private Validator: FastestValidator;
    private compiledValidators = new Map<string, any>();
    constructor() {
        let fastestValidator = loadPackage('fastest-validator', 'ValidationPipe');
        this.Validator = new fastestValidator({
            useNewCustomCheckerFunction: true,
        });
    }

    transform(value: any, metadata: ArgumentMetadata) {

        if (metadata.metatype.name === "Object") {
            return value;
        };
        // let compiled = this.compiledValidators.get(metadata.metatype.name)
        // if (!compiled) {
        //     const schema = getSchema(metadata.metatype);
        //     compiled = this.Validator.compile(schema);
        //     this.compiledValidators.set(metadata.metatype.name, compiled)
        // }
        console.time("V")
        const compiled = getCompiled(metadata.metatype);
        console.timeEnd("V")
        const result = compiled(value)
        if (result !== true) {
            throw new ValidationException(this.formatErrors(result as any, metadata.type));
        } else {
            return value;
        }

    }
    private formatErrors(errors: ValidationError[] = [], type: ArgumentMetadata["type"]) {
        return Object.assign({}, ...errors.map(error => {
            return {
                [error.field]: error.message.replace("field", `field in ${type}`)
            }
        }))
    }
    private isPrimitive(value: unknown): boolean {
        return ['number', 'boolean', 'string'].includes(typeof value);
    }
}


@Catch(ValidationException)
export class ValidationExceptionFilter implements ExceptionFilter {
    catch(exception: ValidationException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.getStatus();
        const env = "dev";
        response
            .status(status)
            .json({
                status: status,
                message: exception.message,
                ...(env == "dev" && { stack: exception.stack }),
                payload: exception.errors
            });
    }
}