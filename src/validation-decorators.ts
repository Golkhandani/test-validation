import { loadPackage } from "@nestjs/common/utils/load-package.util";
const fastestValidator = loadPackage('fastest-validator', 'f-v-decorators');
const SCHEMA_KEY = Symbol("propertyMetadata");
const COMPILE_FUNC = Symbol("compiler");

export function getSchema(target: any): any {
    return Reflect.getMetadata(SCHEMA_KEY, target.prototype);
}

export function getCompiled(target) {
    let compiled = Reflect.getMetadata(COMPILE_FUNC, target.prototype);
    if (!compiled) {
        const v = new fastestValidator({ useNewCustomCheckerFunction: true });
        const s = Reflect.getMetadata(SCHEMA_KEY, target.prototype) || {};
        Reflect.defineMetadata(COMPILE_FUNC, v.compile(s), target.prototype);
        compiled = Reflect.getMetadata(COMPILE_FUNC, target.prototype);
    }
    return compiled;
}

const updateSchema = (target: any, key: string | symbol, options: any): void => {
    const s = Reflect.getMetadata(SCHEMA_KEY, target) || {};
    s[key] = options;
    Reflect.defineMetadata(SCHEMA_KEY, s, target);

};

export function Schema({ strict = false, messages = {}, condition = null } = {}): any {
    return function _Schema<T extends { new(...args: any[]): {} }>(target: T): any {
        updateSchema(target.prototype, "$$strict", strict);
        if (condition !== null) {
            updateSchema(target.prototype, "condition", {
                custom: condition,
                type: "custom",
                optional: true,
            });
        }
        return target;
    };
}

export const decoratorFactory = (mandatory = {}, defaults = {}) => {
    return function (options: any | any[] = {}): any {
        return (target: any, key: string | symbol): any => {
            updateSchema(target, key, { ...defaults, ...options, ...mandatory });
        };
    };
};

export const Field = decoratorFactory({}, {});
export const String = decoratorFactory({ type: "string" }, { empty: false });
export const Boolean = decoratorFactory({ type: "boolean" });
export const Number = decoratorFactory({ type: "number" }, { convert: true });
export const UUID = decoratorFactory({ type: "uuid" });
export const ObjectId = decoratorFactory({ type: "string" }, { pattern: /^[a-f\d]{24}$/i });
export const Email = decoratorFactory({ type: "email" });
export const Date = decoratorFactory({ type: "date" });
export const Enum = decoratorFactory({ type: "enum" });
export const Array = decoratorFactory({ type: "array" });
export const Any = decoratorFactory({ type: "any" });
export const Equal = decoratorFactory({ type: "equal" });

export function Nested(options: any | any[] = {}): any {
    return (target: any, key: string): any => {
        const t = Reflect.getMetadata("design:type", target, key);
        const props = Object.assign({}, getSchema(t));
        const strict = props.$$strict || false;
        delete props.$$strict;
        updateSchema(target, key, { ...options, props, strict, type: "object" });
    };
}