import { Body, Controller, Get, Param, Post, UsePipes } from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { ValidationPipe, Entity2, Entity1, InputParam, Entity3 } from './input.dto';

@Controller()
@ApiTags('test')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Post(":id")
  @ApiResponse({
    status: 200,
    description: 'List of cats',
    type: Entity3,
  })

  getHello(
    @Body() body: Entity2,
    @Param() params: InputParam
  ): string {
    return this.appService.getHello();
  }


  @Post('/orders')
  async addNewOrder() {
    return await this.appService.addNewOrder();
  }
}
