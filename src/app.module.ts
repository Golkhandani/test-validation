import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CategoryModel, OrderItemModel, OrderModel, TranslationEngineModel } from './order.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/nest', { useCreateIndex: true }),
    MongooseModule.forFeature([OrderModel]),
    MongooseModule.forFeature([OrderItemModel]),
    MongooseModule.forFeature([CategoryModel]),
    MongooseModule.forFeature([TranslationEngineModel])
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
