import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    Order,
    OrderDocument,
    OrderStatus,
    OrderPriority, OrderItem, OrderItemDocument, TranslationType, Category, CategoryDocument, TranslationEngineDocument, TranslationEngine, TranslationEngineAvailability, OrderType, Priority
} from './order.schema';

import * as moment from "moment";

@Injectable()
export class AppService {
    constructor(
        @InjectModel(Order.name) private OrderModel: Model<OrderDocument>,
        @InjectModel(OrderItem.name) private OrderItemModel: Model<OrderItemDocument>,
        @InjectModel(Category.name) private CategoryModel: Model<CategoryDocument>,
        @InjectModel(TranslationEngine.name) private TranslationEngineModel: Model<TranslationEngineDocument>) {

    }

    async addNewOrder() {

        // return await this.createEngine();


        // return await this.createCategory();


        // return await this.getEngineById();

        // return await this.getEngines();

        // return await this.createOrder();


        // return await this.getOrdersOfUser()

        return await this.getOrderById();

        return


    }
    private async getOrderById() {
        const [order, items] = await Promise.all([
            this.OrderModel.findById("5f799ca131841f1b08ce1ed1"),
            this.OrderItemModel.find({
                order: "5f799ca131841f1b08ce1ed1",
            }).populate("category")
        ]);
        return Object.assign(order.toObject(), { items });
    }

    private async getOrdersOfUser() {
        return await this.OrderModel.find({ user: "213123" });
    }

    private async getEngines() {
        const query = {};
        const engines = await this.TranslationEngineModel.aggregate([
            {
                $match: query
            },
            {
                $group: {
                    _id: "$from",
                    from: { $first: { lang: "$from" } },
                    to: {
                        $addToSet: {
                            lang: "$to",
                            engineId: "$_id"
                        }
                    },
                    count: { $sum: 1 }
                }
            }
        ]);

        return engines;
    }

    private async createOrder() {
        const orderType = OrderType.OFFICIAL;
        const orderPriority = OrderPriority.NORMAL;
        const engineId = "5f789125245c1e38949c9788";
        const orderItems = [
            {
                file: "hi.com/x1.jpg",
                category: "5f7974439fb0873aa8aa7338",
                word: 1000,
                page: 1000
            }, {
                file: "hi.com/x2.jpg",
                category: "5f796f112743ff31889875dc",
                term: 1
            }
        ];
        const categories = await this.CategoryModel.find({ _id: { $in: orderItems.map(item => item.category) } });
        const engine = await this.TranslationEngineModel.findById(engineId);
        let totalPrice = 0;
        for (let index = 0; index < orderItems.length; index++) {
            const orderItem = orderItems[index];
            const itemCategory = categories.find(item => item._id == orderItem.category);
            let price = 0;
            for (const key in itemCategory.pricing.official) {
                if (Object.prototype.hasOwnProperty.call(itemCategory.pricing.official, key)) {
                    const pricing = itemCategory.pricing.official[key] as Priority<number>;
                    if (!orderItem[key]) {
                        throw new HttpException(orderItem[key] + " doesn't exist", 400);
                    }
                    price = price + pricing[orderPriority] * orderItem[key];
                }
            }
            totalPrice = totalPrice + price;
            orderItem["price"] = price;
        }




        let addedDays = engine[orderType].estimatedTime[orderPriority];
        const todayDay = moment().day();

        if (todayDay === 6 || todayDay === 0) {
            addedDays = addedDays + (todayDay === 0 ? 1 : 2);
        }
        const deliveryDay = moment().add(addedDays, "days").day();
        if (deliveryDay === 6 || deliveryDay === 0) {
            addedDays = addedDays + (deliveryDay === 0 ? 1 : 2);
        }
        const deliveryDate = moment().add(addedDays, "days");




        const newOrder = await this.OrderModel.create({
            engine: engineId,
            user: "213123",
            status: OrderStatus.OPEN,
            priority: orderPriority,
            deliveryTime: deliveryDate as any,
            totalPrice: totalPrice,
            description: "",
            type: orderType
        });
        const newOrderItems = await this.OrderItemModel.insertMany(
            orderItems.map(item => {
                return Object.assign(item, { "order": newOrder._id.toString() });
            })
        );

        return Object.assign(newOrder.toObject(), { items: newOrderItems });
    }

    private async getEngineById() {
        const engineId = "5f789125245c1e38949c9788";
        const categoriesQuery = this.CategoryModel.aggregate([
            {
                $match: {
                    engine: engineId,
                    parent: null
                }
            },
            {
                $set: {
                    cat_id: { $toString: "$_id" }
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: "cat_id",
                    foreignField: "parent",
                    as: "subcategories"
                }
            }
        ]);
        const engineQuery = this.TranslationEngineModel.findById(engineId);
        const [engine, categories] = await Promise.all([
            engineQuery,
            categoriesQuery
        ]);
        return Object.assign(engine.toObject(), { categories });
    }

    private async createCategory() {
        const newCategory = await this.CategoryModel.create({
            engine: "5f789125245c1e38949c9788",
            parent: "5f796f034d6a6e3b8cc34d38",
            title: "certificate",
            pricing: {
                official: {
                    word: { normal: 1, urgent: 3 },
                    page: { normal: 1, urgent: 3 }
                }
            }
        });
        return newCategory;
    }

    private async createEngine() {
        const newEngine = await this.TranslationEngineModel.create({
            title: "",
            description: "",
            availability: TranslationEngineAvailability.AVAILABLE,
            from: "Persian",
            to: "English",
            official: {
                estimatedTime: {
                    normal: 2,
                    urgent: 1
                }
            },
            unofficial: null,
            notarized: null
        });
        return newEngine;
    }

    getHello(): string {
        return 'Hello World!';
    }
}




/**
 * {
    "notarized": null,
    "unofficial": null,
    "official": {
        "estimatedTime": {
            "normal": 2,
            "urgent": 1
        }
    },
    "_id": "5f789125245c1e38949c9788",
    "title": "",
    "description": "",
    "availability": "AVAILABLE",
    "from": "Persian",
    "to": "French",
    "createdAt": "2020-10-03T14:56:37.322Z",
    "updatedAt": "2020-10-03T14:56:37.322Z",
    "categories": [
        {
            "_id": "5f789158ae852e2a54ae0a1b",
            "engine": "5f789125245c1e38949c9788",
            "title": "transcript",
            "pricing": {
                "official": {
                    "word": {
                        "normal": 1,
                        "urgent": 3
                    },
                    "page": {
                        "normal": 1,
                        "urgent": 3
                    }
                }
            },
            "createdAt": "2020-10-03T14:57:28.646Z",
            "updatedAt": "2020-10-03T14:57:28.646Z"
        },
        {
            "_id": "5f7891a85a128f3f80198049",
            "engine": "5f789125245c1e38949c9788",
            "title": "transcript",
            "pricing": {
                "official": {
                    "word": {
                        "normal": 1,
                        "urgent": 3
                    },
                    "page": {
                        "normal": 1,
                        "urgent": 3
                    }
                }
            },
            "createdAt": "2020-10-03T14:58:48.908Z",
            "updatedAt": "2020-10-03T14:58:48.908Z"
        }
    ]
}
 */